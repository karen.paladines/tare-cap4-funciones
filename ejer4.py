# ¿Cuál es la utilidad de la palabra clave “def” en Python?

# autora = "Karen Paladines"
# email = "karen.paladines@unl.edu.ec"

def verdadera():
    print("Indica el comienzo de una función")

verdadera()

# presenta: Es una jerga que signiﬁca “este código es realmente estupendo”
# presenta: Indica el comienzo de una función
# presenta: Indica que la siguiente sección de código indentado debe ser almacenada para usarla más tarde
# presenta: b y c son correctas ambas
# presenta: Ninguna de las anteriores
