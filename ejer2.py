# Desplaza la última línea del programa anterior hacia arriba, de modo que la llamada a la
# función aparezca antes que las deﬁniciones. Ejecuta el programa y observa qué mensaje
# de error obtienes.

# autora = "Karen Paladines"
# email = "karen.paladines@unl.edu.ec"

def muestra_estribillo() :
    print('Hola soy Sulema, tu amiga.')
    print('Quiero ser tú mi amigo.')


def respite_estribillo() :
    muestra_estribillo()
    muestra_estribillo()

respite_estribillo()
print('Hola soy Sulema, tu amiga.')
print('Quiero ser tú mi amigo.')

#presenta los mensaje de la funcion repite_estribillo anidada en ls función muestra_estribillo
#y las sentencias de la función muetsra_estribillo sin llamar a la función.
