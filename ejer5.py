# ¿Qué mostrará en pantalla el siguiente programa Python?

# autora = "Karen Paladines"
# email = "karen.paladines@unl.edu.ec"

def fred() :
    print("Zap")

def jane ():
    print("ABC")

jane()
fred()
jane()

# presenta: ABC
# presenta: Zap
