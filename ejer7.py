# Reescribe el programa de caliﬁcaciones del capítulo anterior usando una función
# llamada calcula_calificacion, que reciba una puntuación como parámetro y devuelva una caliﬁcación como cadena.

# autora = "Karen Paladines"
# email = "karen.paladines@unl.edu.ec"

def calcula_calificación (calif):
    if calif >=0 and calif <=1.0:
        if calif >= 0.8:
            print("Sobresaliente")
        elif calif >= 0.7:
            print("Notable")
        elif calif >= 0.6:
            print("Bien")
        elif calif <= 0.5:
            print("Insuficiente")
    else:
        print("La puntuación no es correcta")

try:
    punt = float(input("Introduzca una puntuacion"))
    calcula_calificación(punt)
except:
    print("La puntuación incorrecta")
