#  Reescribe el programa de cálculo del salario,
#  con tarifa-ymedia para las horas extras, y crea una función
#  llamada calculo_salario que reciba dos parámetros (horas y tarifa).

# autora = "Karen Paladines"
# email = "karen.paladines@unl.edu.ec"


def calculo_salario (sala,tarif):
    if sala <= 45:
        total = sala + tarif
        print("La cantidad recibida es", total, "$")
    elif sala >= 46:
        sala -= 40
        total = 40 * tarif
        incremen = 1.5 * (sala * tarif)
        total += incremen
        print("La cantidad recibida es", "$")
    else:
        print("La cantidad ingresada es correcta")
try:
    saldo = float(input("Ingrese el numero de horas"))
    tarifa = float(input("Ingrese el monto por horas"))
    calculo_salario(saldo,tarifa)
except:
     print("La cantidad ingresada es incorrecta")